Docker install Ansible Role
=========

Role to install Docker engine on target hosts.

Variables
----------------

### Install docker compose with docker
```
docker_install_compose: true
```

Testing with Molecule
----------------
The default distribution for tests is ubuntu:22.04. You can override th with setting the environment variable MOLECULE_DISTRO
```
MOLECULE_DISTRO=debian:11 molecule test
```

Example Playbook
----------------

    - hosts: servers
      roles:
         - { role: sample.docker }

License
-------

GPLv3

Author Information
------------------

* Kirill Fedorov as a challege for ClassBook project https://deusops.com/classbook
